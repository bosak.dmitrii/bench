﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Benchmark
{
  [MemoryDiagnoser]
  public class FindUniqueElement
  {
    private Random _random = new Random(1);
    public int[] Data1;
    public int[] Data2;
    public int[] Data3;
    public int[] Data4;
    public int[] Data5;
    public int[] OriginalData;

    public int UniqueItem;

    [Params(10, 10_000)]
    public int N;

    [Params(UniquePlacement.End, UniquePlacement.Middle, UniquePlacement.Start)]
    public UniquePlacement Placement;

    [GlobalSetup]
    public void Setup()
    {      
      UniqueItem = Placement switch
      {
        UniquePlacement.Start => FillWithUniqueIsFirst(),
        UniquePlacement.Middle => FillWithUniqueInTheMiddle(),
        UniquePlacement.End => FillWithUniqueInTheEnd(),
        _ => throw new NotImplementedException()
      };
      Data1 = new int[OriginalData.Length];
      OriginalData.CopyTo(Data1, 0);
      Data2 = new int[OriginalData.Length];
      OriginalData.CopyTo(Data2, 0);
      Data3 = new int[OriginalData.Length];
      OriginalData.CopyTo(Data3, 0);
      Data4 = new int[OriginalData.Length];
      OriginalData.CopyTo(Data4, 0);
      Data5 = new int[OriginalData.Length];
      OriginalData.CopyTo(Data5, 0);
    }

    private int[] Generate()
    {
      var buffer = new int[N / 2];

      for (int i = 0; i < N / 2; i++)
      {
        buffer[i] = _random.Next();
      }
      return buffer;
    }

    private int CreateUnique()
    {
      var unique = _random.Next();
      while (OriginalData.Any(item => item == unique))
        unique = _random.Next();
      return unique;
    }

    public int FillWithUniqueInTheEnd()
    {
      OriginalData = new int[N + 1];
      var buffer = Generate();
      buffer.CopyTo(OriginalData, 0);
      buffer.CopyTo(OriginalData, N / 2);
      return OriginalData[N] = CreateUnique();
    }

    public int FillWithUniqueInTheMiddle()
    {
      OriginalData = new int[N + 1];
      var buffer = Generate();
      buffer.CopyTo(OriginalData, 0);
      buffer.CopyTo(OriginalData, N / 2 + 1);
      return OriginalData[N/2] = CreateUnique();
    }

    public int FillWithUniqueIsFirst()
    {
      OriginalData = new int[N + 1];
      var buffer = Generate();
      buffer.CopyTo(OriginalData, 1);
      buffer.CopyTo(OriginalData, N / 2 + 1);
      return OriginalData[0] = CreateUnique();
    }

    [Benchmark(Baseline = true)]
    public int LinqInsideLinq()
    {
      int res = Data1.Where(x => Data1.Count(i => i == x) <= 1).FirstOrDefault();
      return res;
    }

    [Benchmark]
    public int LoopWithHandSerach()
    {
      Array.Sort(Data2);
      int currentUniqNum = Data2[0];
      int countOfUniqNum = 1;

      for (int i = 1; i < Data2.Length; i++)
      {
        if (currentUniqNum != Data2[i])
        {
          if (countOfUniqNum <= 1) break;
          else
          {
            countOfUniqNum = 0;
            currentUniqNum = Data2[i];
          }
        }
        countOfUniqNum++;
      }
      return currentUniqNum;
    }

    [Benchmark]
    public int Dictionary()
    {
      var grouped = new Dictionary<int, int>();

      foreach (var item in Data3)
      {
        if (!grouped.Any(x => x.Key == item))
          grouped[item] = 1;
        else
          grouped[item] += 1;
      }

      var result = grouped.First(KeyValuePair => KeyValuePair.Value == 1);
      return result.Key;
    }

    [Benchmark]
    public int GroupBy()
    {
      var result = Data4.GroupBy(x => x).First(x => x.Count() == 1);
      return result.Key;
    }

    [Benchmark]
    public int Alexandr()
    {
      Array.Sort(Data5);

      int bingo = 0;

      if (Data5[0] != Data5[1])
      {
        bingo = Data5[0];
      }
      else if (Data5[Data5.Length - 1] != Data5[Data5.Length - 2])
      {
        bingo = Data5[Data5.Length - 1];
      }
      else
      {
        for (int i = 1; i < Data5.Length - 1; i++)
        {
          if (Data5[i] != Data5[i + 1] && Data5[i] != Data5[i - 1])
          {
            bingo = Data5[i];
          }
        }
      }
      return bingo;
    }
  }
}
