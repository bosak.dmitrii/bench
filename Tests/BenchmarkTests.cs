using Benchmark;
using NUnit.Framework;
using System;
using FluentAssertions;
using System.Linq;

namespace Tests
{
  public class BenchmarkTests
  {
    FindUniqueElement test;

    [SetUp]
    public void Setup()
    {
      test = new FindUniqueElement();
      test.N = 100;
    }

    [Test]
    public void Test_LoopWithHandSerach_WhenUniqueInTheEnd()
    {
      // arrange
      test.Placement = UniquePlacement.End;
      test.Setup();

      // act
      var result = test.LoopWithHandSerach();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LoopWithHandSerach_WhenUniqueInTheMiddle()
    {
      // arrange
      test.Placement = UniquePlacement.Middle;
      test.Setup();

      // act
      var result = test.LoopWithHandSerach();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LoopWithHandSerach_WhenUniqueInTheStart()
    {
      // arrange
      test.Placement = UniquePlacement.Start;
      test.Setup();

      // act
      var result = test.LoopWithHandSerach();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LoopWithHandSerach_WhenUniqueItemInTheEnd()
    {
      // arrange
      test.Placement = UniquePlacement.End;
      test.Setup();

      // act
      var result = test.LoopWithHandSerach();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LoopWithHandSerach_WhenUniqueItemInTheMiddle()
    {
      // arrange
      test.Placement = UniquePlacement.Middle;
      test.Setup();

      // act
      var result = test.LoopWithHandSerach();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LoopWithHandSerach_WhenUniqueItemInTheStart()
    {
      // arrange
      test.Placement = UniquePlacement.Start;
      test.Setup();

      // act
      var result = test.LoopWithHandSerach();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LinqInsideLinq_WhenUniqueItemInTheEnd()
    {
      // arrange
      test.Placement = UniquePlacement.End;
      test.Setup();

      // act
      var result = test.LinqInsideLinq();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LinqInsideLinq_WhenUniqueItemInTheMiddle()
    {
      // arrange
      test.Placement = UniquePlacement.Middle;
      test.Setup();

      // act
      var result = test.LinqInsideLinq();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_LinqInsideLinq_WhenUniqueItemInTheStart()
    {
      // arrange
      test.Placement = UniquePlacement.Start;
      test.Setup();

      // act
      var result = test.LinqInsideLinq();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Dictionary_WhenUniqueItemInTheEnd()
    {
      // arrange
      test.Placement = UniquePlacement.End;
      test.Setup();

      // act
      var result = test.Dictionary();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Dictionary_WhenUniqueItemInTheMiddle()
    {
      // arrange
      test.Placement = UniquePlacement.Middle;
      test.Setup();

      // act
      var result = test.Dictionary();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Dictionary_WhenUniqueItemInTheStart()
    {
      // arrange
      test.Placement = UniquePlacement.Start;
      test.Setup();

      // act
      var result = test.Dictionary();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_GroupBy_WhenUniqueItemInTheEnd()
    {
      // arrange
      test.Placement = UniquePlacement.End;
      test.Setup();

      // act
      var result = test.GroupBy();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_GroupBy_WhenUniqueItemInTheMiddle()
    {
      // arrange
      test.Placement = UniquePlacement.Middle;
      test.Setup();

      // act
      var result = test.GroupBy();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_GroupBy_WhenUniqueItemInTheStart()
    {
      // arrange
      test.Placement = UniquePlacement.Start;
      test.Setup();

      // act
      var result = test.GroupBy();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Alexandr_WhenUniqueItemInTheEnd()
    {
      // arrange
      test.Placement = UniquePlacement.End;
      test.Setup();

      // act
      var result = test.Alexandr();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Alexandr_WhenUniqueItemInTheMiddle()
    {
      // arrange
      test.Placement = UniquePlacement.Middle;
      test.Setup();

      // act
      var result = test.Alexandr();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Alexandr_WhenUniqueItemInTheStart()
    {
      // arrange
      test.Placement = UniquePlacement.Start;
      test.Setup();

      // act
      var result = test.Alexandr();

      // assert
      result.Should().Be(test.UniqueItem);
    }

    [Test]
    public void Test_Setup_UniqueInTheEnd()
    {
      // arrange
      var test = new FindUniqueElement();
      test.N = 100;

      //act
      test.Placement = UniquePlacement.End;
      test.Setup();

      //assert
      Assert.That(test.OriginalData.All(x => x > 0));
      Assert.That(test.OriginalData.Last() == test.GroupBy());
      Assert.That(test.OriginalData.Last() == test.UniqueItem);
    }

    [Test]
    public void Test_Setup_UniqueInTheMiddle()
    {
      // arrange
      var test = new FindUniqueElement();
      test.N = 100;

      //act
      test.Placement = UniquePlacement.Middle;      
      test.Setup();

      //assert
      Assert.That(test.OriginalData.All(x => x > 0));
      Assert.That(test.OriginalData[test.OriginalData.Length / 2] == test.GroupBy());
      Assert.That(test.OriginalData[test.OriginalData.Length / 2] == test.UniqueItem);
    }

    [Test]
    public void Test_Setup_UniqueIsFirst()
    {
      // arrange
      var test = new FindUniqueElement();
      test.N = 100;

      //act
      test.Placement = UniquePlacement.Start;
      test.Setup();

      //assert
      Assert.That(test.OriginalData.All(x => x > 0));
      Assert.That(test.OriginalData[0] == test.GroupBy());
      Assert.That(test.OriginalData[0] == test.UniqueItem);
    }
  }
}